/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/		
		function enterUserInfo(){
			let userFullName = prompt ('Enter your Full name');
			console.log('Hello, '+ userFullName);

			let userAge = prompt('Enter your Age');
			console.log('You are ' + userAge + ' years old.');

			let userLocation =prompt('Enter your Location');
			console.log('You live in ' + userLocation);
		}

		enterUserInfo();
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
		
*/

	//second function here:
	function userFavorite(){
			let favoriteBandOrMusicalArtist = ['Silent Sanctuary', 'Callalily', 'December Avenue', 'Maroon 5', 'Joji'];
			console.log('1. ' + favoriteBandOrMusicalArtist[0]);
			console.log('2. ' + favoriteBandOrMusicalArtist[1]);
			console.log('3. ' + favoriteBandOrMusicalArtist[2]);
			console.log('4. ' + favoriteBandOrMusicalArtist[3]);
			console.log('5. ' + favoriteBandOrMusicalArtist[4]);
		}

		userFavorite();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function userFavorite2(){
			let favoriteMovie = ['Iron Man', 'Avengers: Endgame', 'Avengers: Infinity War ', 'Iron Man 2', 'Iron Man 3'];
			console.log('1. ' + favoriteMovie[0]);
			console.log('Rotten Tomatoes Rating: 91%');
			console.log('2. ' + favoriteMovie[1]);
			console.log('Rotten Tomatoes Rating: 90%');
			console.log('3. ' + favoriteMovie[2]);
			console.log('Rotten Tomatoes Rating: 91%');
			console.log('4. ' + favoriteMovie[3]);
			console.log('Rotten Tomatoes Rating: 71%');
			console.log('5. ' + favoriteMovie[4]);
			console.log('Rotten Tomatoes Rating: 78%');
		}

		userFavorite2();

		function addFriendsName(){
			alert('Hi! Please add the names of your friends.');
		};

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

alert("Hi! Please add the names of your friends.");

let printFriends = function(){
	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

